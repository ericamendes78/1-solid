﻿using System;

namespace SOLID.SRP
{
    public class Account
    {

        #region "Properties"
        public long AccountId { get; private set; }
        public int Agency { get; private set; }
        public Holder Holder { get; private set; }
        public decimal Balance { get; private set; }
        public AccountType Type { get; internal set; }

        #endregion

        #region "Constructors"
        public Account(int Agency, Holder Holder, decimal Balance, AccountType Type)
        {
            this.Agency = Agency;
            this.Holder = Holder;
            this.Balance = Balance;
            this.Type = Type;

            var random = new Random();
            AccountId = random.Next(1, 100);
        }
        public Account(int Agency, Holder Holder, AccountType Type) : this(Agency, Holder, 0, Type)
        {
        }
        #endregion

        #region "Methods"
        public decimal GetTaxAdministrative()
        {
            decimal tax = 0;
            if (Type == AccountType.CHECKING)
            {
                tax = 0.3M;
            }
            else if (Type == AccountType.SAVINGS)
            {
                tax = 0.1M;
            }
            else if (Type == AccountType.INVESTIMENT)
            {
                tax = 0.5M;
            }

            return tax;

        }
        public bool IsValid()
        {
            if (Balance < 5)
            {
                Console.WriteLine("Insulfficient balance for opening the account.");
                return false;
            }

            return true;
        }

        #endregion 
    }
}
