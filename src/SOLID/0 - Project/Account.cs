﻿using System;
using System.Data.SqlClient;

namespace SOLID.Project
{
    public class Account
    {
        #region "Properties"
        public long AccountId { get; private set; }
        public int Agency { get; private set; }
        public string Name { get; private set; }
        public string CPF { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Email { get; private set; }
        public long Telephone { get; private set; }
        public decimal Balance { get; private set; }
        public AccountType Type { get; private set; }

        #endregion

        #region "Constructors"
        public Account(int Agency, string Name, string CPF, DateTime BirthDate, string Email, long Telephone, decimal Balance, AccountType Type)
        {
            this.Agency = Agency;
            this.Name = Name;
            this.CPF = CPF;
            this.BirthDate = BirthDate;
            this.Email = Email;
            this.Telephone = Telephone;
            this.Balance = Balance;
            this.Type = Type;

            var random = new Random();
            AccountId = random.Next(1, 100);

            OpenAccount();

        }

        public Account(int Agency, string Name, string CPF, DateTime BirthDate, string Email, long Telephone, AccountType Type) : this(Agency, Name, CPF, BirthDate, Email,Telephone,0, Type)
        {
        }

        #endregion

        #region "Methods"

        public bool IsValid()
        {

            if (CPF.Length != 11)
            {
                Console.WriteLine("CPF invalid.");
                return false;
            }

            if (BirthDate == DateTime.MinValue)
            {
                Console.WriteLine("Invalid birthday date.");
                return false;
            }

            if (Email.Length < 10 || !Email.Contains("@"))
            {
                Console.WriteLine("E-mail invalid to receive invoice.");
                return false;
            }

            if (Telephone.ToString().Length < 9)
            {
                Console.WriteLine("Telephone invalid to receive invoice by SMS.");
                return false;
            }

            if (Balance < 5)
            {
                Console.WriteLine("Insulfficient balance for opening the ");
                return false;
            }

            return true;

        }
        public void OpenAccount()
        {
            if (IsValid())
            {
                SaveHolder();
                SaveAccount();
            }
            else
            {
                Console.WriteLine("Check the data provided for opening an account.");
            }

        }
        public decimal GetTaxAdministrative()
        {
            decimal tax = 0;
            if (Type == AccountType.CHECKING)
            {
                tax = 0.3M;
            }
            else if (Type == AccountType.SAVINGS)
            {
                tax = 0.1M;
            }
            else if (Type == AccountType.INVESTIMENT)
            {
                tax = 0.5M;
            }

            return tax;

        }
        public void SaveAccount()
        {

            using (var connection = new SqlConnection("connectionString"))
            {
                var command = new SqlCommand();

                command.Connection = connection;
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "INSERT INTO Account (AccountId,Agency,Balance,HolderCPF,Type) VALUES (@AccountId, @Agency, @Balance, @HolderCPF, @Type)";
                command.Parameters.AddWithValue("AccountId", AccountId);
                command.Parameters.AddWithValue("Agency", Agency);
                command.Parameters.AddWithValue("Balance", Balance);
                command.Parameters.AddWithValue("HolderCPF", CPF);
                command.Parameters.AddWithValue("Type", Type);

                connection.Open();

                command.ExecuteNonQuery();

            }

        }
        public void SaveHolder()
        {

            using (var connection = new SqlConnection("connectionString"))
            {
                var command = new SqlCommand();

                command.Connection = connection;
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "INSERT INTO Cliente (CPF,Name,BirthDate,Telephone,Email) VALUES (@CPF, @Name, @BirthDate, @Telephone, @Email)";

                command.Parameters.AddWithValue("CPF", CPF);
                command.Parameters.AddWithValue("Name", Name);
                command.Parameters.AddWithValue("BirthDate", BirthDate);
                command.Parameters.AddWithValue("Telephone", Telephone);
                command.Parameters.AddWithValue("Email", Email);

                connection.Open();

                command.ExecuteNonQuery();

            }

        }
       
        #endregion

    }
}
