﻿using System.Data.SqlClient;

namespace SOLID.LSP.Repositories
{
    public class AccountRepository
    {
        #region "Methods"
        public void Save(Account account)
        {
            using (var connection = new SqlConnection("connectionString"))
            {
                var command = new SqlCommand();

                command.Connection = connection;
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "INSERT INTO Account (AccountId,Agency,Balance,HolderCPF,Type) VALUES (@AccountId, @Agency, @Balance, @HolderCPF, @Type)";
               
                command.Parameters.AddWithValue("AccountId", account.AccountId);
                command.Parameters.AddWithValue("Agency", account.Agency);
                command.Parameters.AddWithValue("Balance", account.Balance);
                command.Parameters.AddWithValue("HolderCPF", account.Holder.CPF);
                command.Parameters.AddWithValue("Type", account.Type);

                connection.Open();

                command.ExecuteNonQuery();

            }
        }

        #endregion 
    }
}
