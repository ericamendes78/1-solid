﻿using System;

namespace SOLID.LSP
{
    public abstract class Account
    {
        #region "Properties"
        public long AccountId { get; private set; }
        public int Agency { get; private set; }
        public Holder Holder { get; private set; }
        public decimal Balance { get; private set; }
        public AccountType Type { get; private set; }

        #endregion

        #region "Constructors"

        public Account(int Agency, Holder Holder, decimal Balance, AccountType Type)
        {
            this.Agency = Agency;
            this.Holder = Holder;
            this.Balance = Balance;
            this.Type = Type;

            var random = new Random();
            AccountId = random.Next(1, 100);
        }
        public Account(int Agency, Holder Holder, AccountType Type) : this(Agency, Holder, 0, Type)
        {
        }

        #endregion

        #region "Methods"
        public abstract decimal GetTaxAdministrative();

        public abstract bool IsValid();

        #endregion

    }
}
