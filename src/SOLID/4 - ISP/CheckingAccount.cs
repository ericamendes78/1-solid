﻿using System;

namespace SOLID.ISP
{
    public class CheckingAccount : Account
    {

        #region "Constructors"
        public CheckingAccount(int Agency, Holder Holder, decimal Balance) : base(Agency, Holder, Balance, AccountType.CHECKING)
        {
        }

        public CheckingAccount(int Agency, Holder Holder) : base(Agency, Holder, 0)
        {

        }

        #endregion

        #region "Methods"
        public override decimal GetTaxAdministrative()
        {
            return 0.3M;
        }
        public override bool IsValid()
        {
            if (Balance < 50)
            {
                Console.WriteLine("Insulfficient balance for opening the account.");
                return false;
            }

            return true;
        }

        #endregion 
    }
}
