﻿using System;

namespace SOLID.ISP
{
    public class SavingsAccount : Account
    {
        #region "Constructors"
        public SavingsAccount(int Agency, Holder Holder, decimal Balance) : base(Agency, Holder, Balance, AccountType.SAVINGS)
        {
        }
        public SavingsAccount(int Agency, Holder Holder) : base(Agency, Holder, 0)
        {
        }

        #endregion

        #region "Methods"
        public override decimal GetTaxAdministrative()
        {
            return 0.1M;
        }

        public override bool IsValid()
        {
            if (Balance < 1)
            {
                Console.WriteLine("Insulfficient balance for opening the account.");
                return false;
            }

            return true;
        }

        #endregion 

    }
}
