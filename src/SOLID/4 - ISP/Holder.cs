﻿using System;

namespace SOLID.ISP
{
    public class Holder
    {
        #region "Properties"
        public string Name { get; private set; }
        public string CPF { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Email { get; private set; }
        public long Telephone { get; private set; }

        #endregion

        #region "Constructors"

        public Holder(string Name, string CPF, DateTime BirthDate, string Email, long Telephone)
        {
            this.Name = Name;
            this.CPF = CPF;
            this.BirthDate = BirthDate;
            this.Email = Email;
            this.Telephone = Telephone;
        }

        #endregion 

        #region "Methods"
        public bool IsValid()
        {
            if (CPF.Length != 11)
            {
                Console.WriteLine("CPF inválido.");
                return false;
            }

            if (BirthDate == DateTime.MinValue)
            {
                Console.WriteLine("Invalid birthday date.");
                return false;
            }

            if (Email.Length < 10 || !Email.Contains("@"))
            {
                Console.WriteLine("E-mail invalid to receive invoice.");
                return false;
            }

            if (Telephone.ToString().Length < 9)
            {
                Console.WriteLine("Telephone invalid to receive invoice by SMS.");
                return false;
            }

            return true;

        }
        #endregion 
    }
}
