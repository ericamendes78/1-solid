﻿namespace SOLID.ISP.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Save(TEntity entity);
    }
}
