﻿namespace SOLID.ISP.Interfaces
{
    public interface IHolderRepository : IRepository<Holder>
    {
    }
}
