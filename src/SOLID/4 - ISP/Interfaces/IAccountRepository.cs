﻿namespace SOLID.ISP.Interfaces
{
    public interface IAccountRepository : IRepository<Account>
    {
    }
}
