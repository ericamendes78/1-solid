﻿using SOLID.DIP.Interfaces;
using System.Data.SqlClient;

namespace SOLID.DIP.Repositories
{
    public class HolderRepository : IHolderRepository
    {
        #region "Methods"
        public void Save(Holder holder)
        {
            using (var connection = new SqlConnection("connectionString"))
            {
                var command = new SqlCommand();

                command.Connection = connection;
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = "INSERT INTO Cliente (CPF,Name,BirthDate,Telephone,Email) VALUES (@CPF, @Name, @BirthDate, @Telephone, @Email)";

                command.Parameters.AddWithValue("CPF", holder.CPF);
                command.Parameters.AddWithValue("Name", holder.Name);
                command.Parameters.AddWithValue("BirthDate", holder.BirthDate);
                command.Parameters.AddWithValue("Telephone", holder.Telephone);
                command.Parameters.AddWithValue("Email", holder.Email);

                connection.Open();

                command.ExecuteNonQuery();

            }    
        }

        #endregion 
    }
}
