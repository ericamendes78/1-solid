﻿namespace SOLID.DIP.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Save(TEntity entity);

    }
}
