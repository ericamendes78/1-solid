﻿namespace SOLID.DIP.Interfaces
{
    public interface IOpenAccountService
    {
        void OpenAccount(Account account);
    }
}
