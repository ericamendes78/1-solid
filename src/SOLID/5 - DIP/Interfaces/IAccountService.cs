﻿namespace SOLID.DIP.Interfaces
{
    public interface IAccountService
    {
        void CreateAccount(Account account);
    }
}
