﻿namespace SOLID.DIP.Interfaces
{
    public interface IAccountRepository : IRepository<Account>
    {
    }
}
