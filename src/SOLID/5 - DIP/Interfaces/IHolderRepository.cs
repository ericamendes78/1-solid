﻿namespace SOLID.DIP.Interfaces
{
    public interface IHolderRepository : IRepository<Holder>
    {

    }
}
