﻿namespace SOLID.DIP.Interfaces
{
    public interface IHolderService
    {
        void CreateHolder(Holder holder);
    }
}
