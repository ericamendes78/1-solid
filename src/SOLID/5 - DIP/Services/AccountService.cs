﻿using SOLID.DIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.DIP.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _respository;

        public AccountService(IAccountRepository respository)
        {
            _respository = respository;
        }

        public void CreateAccount(Account account)
        {

            if (!account.IsValid())
            {
                Console.WriteLine("Invalid account opening data");
            }
            else
            {
                _respository.Save(account);
            }

        }
    }
}
