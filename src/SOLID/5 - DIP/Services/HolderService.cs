﻿using SOLID.DIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.DIP.Services
{
    public class HolderService : IHolderService
    {
        private readonly IHolderRepository _respository;

        public HolderService(IHolderRepository respository)
        {
            _respository = respository;
        }

        public void CreateHolder(Holder holder)
        {

            if (!holder.IsValid())
            {
                Console.WriteLine("Invalid data for customer registration.");
            }
            else
            {
                _respository.Save(holder);
            }

        }
    }
}
