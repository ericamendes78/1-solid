﻿using SOLID.DIP.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.DIP.Services
{
    public class OpenAccountService : IOpenAccountService
    {
        private readonly IAccountService _accountService;
        private readonly IHolderService _holderService;

        public OpenAccountService(IAccountService accountService, IHolderService holderService)
        {
            _accountService = accountService;
            _holderService = holderService;

        }

        public void OpenAccount(Account account)
        {
            _holderService.CreateHolder(account.Holder);
            _accountService.CreateAccount(account);

        }
    }
}
