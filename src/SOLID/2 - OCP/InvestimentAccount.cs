﻿using System;

namespace SOLID.OCP
{
    public class InvestimentAccount : Account

    {
        #region "Constructors"
        public InvestimentAccount(int Agency, Holder Holder, decimal Balance) : base(Agency, Holder, Balance, AccountType.INVESTIMENT)
        {
        }

        public InvestimentAccount(int Agency, Holder Holder) : base(Agency, Holder, 0)
        {
        }

        #endregion

        #region "Methods"
        public override decimal GetTaxAdministrative()
        {
            return 0.5M;
        }

        public override bool IsValid()
        {
            if (Balance < 10000)
            {
                Console.WriteLine("Insulfficient balance for opening the account.");
                return false;
            }

            return true;
        }

        #endregion 
    }
}
